from selene.support.shared import browser
from selene import by, be, have


def test_can_login_with_valid_cred():
    browser.config.browser_name = 'chrome'
    browser.config.base_url = 'https://rozetka.com.ua/'
    browser.config.timeout = 4

    browser.open('/')
    browser.element(by.css("div.js-mobile-user-menu svg")).click()
    browser.element(by.css(".menu-main li:nth-child(2)")).click()
    browser.element(by.css("#auth_email")).set_value("bzhest@gmail.com")
    browser.element(by.css("#auth_pass")).set_value("NMetAY01")
    browser.element(by.css("div .auth-modal__submit")).click()
    browser.element(by.css("div.js-mobile-user-menu svg")).click()

    browser.element(by.css(".menu-main li:nth-child(2)")).should(have.text('Андрей'))
    # js-mobile-user-menu svg
    # browser.element(by.name('q')).should(be.blank) \
        # .type('selenium').press_enter()
    # browser.all('.srg .g').should(have.size(10)) \
        # .first.should(have.text('Selenium automates browsers'))