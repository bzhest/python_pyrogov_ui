
from selene import by, be, have

from src.pages.LoginPage import LoginPage
from src.pages.MainPage import MainPage


def test_can_login_with_valid_cred(browser):

    browser.open('/')
    browser.element(by.css("button.header-topline__user-link")).click()
    browser.element(by.css("#auth_email")).set_value("bzhest@gmail.com")
    browser.element(by.css("#auth_pass")).set_value("NMetAY01")
    browser.element(by.css("div .auth-modal__submit")).click()
    browser.element(by.css("a.header-topline__user-link")).should(have.text('Андрей'))


def test_can_login_with_valid_cred_page_obj(app):

    (app
        .login_page()
        .open()
        .login_as("bzhest@gmail.com", "NMetAY01")
     )

    app.main_page().user_name().should(have.exact_text('Андрей'))
