import requests
from selene import have

from src.api.login import do_login_with_api


def test_can_do_api_call(app, faker):
    fake_name = faker.name()
    do_login_with_api()
    app.auth()
    app.main_page().open()\
    .episode_name().should(have.exact_text("Andrii")