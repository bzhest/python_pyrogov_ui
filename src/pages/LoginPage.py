import allure

from src.pages.Page import Page


class LoginPage(Page):

    def __init__(self, browser):
        super().__init__(browser)

    @allure.step("Open login page")
    def open(self):
        self.browser.open("/")
        return self

    @allure.step("Login as {1} {2}")
    def login_as(self, login, password):
        self.browser.element("button.header-topline__user-link").click()
        self.browser.element("#auth_email").set_value(login)
        self.browser.element("#auth_pass").set_value(password)
        self.browser.element("div .auth-modal__submit").click()
