from selene.core.entity import SeleneElement

from src.pages.Page import Page


class MainPage(Page):

    def __init__(self, browser):
        super().__init__(browser)

    def open(self):
        self.browser.open('/list')
        return self

    def user_name(self):
        return self.browser.element("a.header-topline__user-link")

    def episode_name(self) -> SeleneElement:
        return self.browser.element("span.episode-name")
